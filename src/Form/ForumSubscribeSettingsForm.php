<?php

namespace Drupal\forum_subscribe\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Forum Subscribe settings for this site.
 */
class ForumSubscribeSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'forum_subscribe.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'forum_subscribe_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $flag_service = \Drupal::service('flag');
    $all_flags = $flag_service->getAllFlags();
    $anon = \Drupal\user\Entity\Role::load('anonymous');
    $bundle_checkboxes = [];
    foreach($all_flags as $flag) {
      if (!$anon->hasPermission("flag " . $flag->id)) {
        $bundle_checkboxes[$flag->id] = $flag->label;
      }
    }

    $config = $this->config(static::SETTINGS);
    $flag_types = $config->get('flag-types');

    $form['flag_types_list'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t("Flag types"),
      '#default_value' => $flag_types,
      '#options' => $bundle_checkboxes,
      '#description' => $this->t("Flag types enabled here will let Drupal know which flagged nodes' new comments should trigger sending emails to all subscribers. Only flags not available to anonymous users can be selected!"),
    ];

    $form['mail_template'] = [
      '#type' => 'container',
      '#title' => 'Mail template',
      '#tree' => TRUE,
    ];

    $form['mail_template']['subject'] = [
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $config->get('mail-template.subject'),
      '#rows' => 15,
      '#token_types' => [
        'node', 'user',
      ],
      '#element_validate' => ['token_element_validate'],
      '#description' => $this->t("The subject of the email notification. This field supports tokens."),
      '#required' => TRUE,
    ];

    $form['mail_template']['body'] = [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => $config->get('mail-template.body'),
      '#resizable' => 'vertical',
      '#rows' => 15,
      '#token_types' => [
        'comment', 'node', 'user',
      ],
      '#element_validate' => ['token_element_validate'],
      '#description' => $this->t("The body of the email notification. This field supports tokens."),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
    /* public function validateForm(array &$form, FormStateInterface $form_state) {
      if (!in_array($form_state->getValue('flag_types_list'), array_keys(\Drupal::service('flag')->getAllFlags('commerce_product')))) {
        $form_state->setErrorByName('flag_types_list', $this->t('The provided flag does not exist or is not part of the product bundle!'));
      }
    } */

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $flag_list = $form_state->getValue('flag_types_list');
    $subject = $form_state->getValue(['mail_template', 'subject',]);
    $body = $form_state->getValue(['mail_template', 'body',]);
    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::SETTINGS);
    // Set the submitted configuration setting.
    $config->set('flag-types', array_keys(array_filter($flag_list)));
    $config->set('mail-template.subject', $subject);
    $config->set('mail-template.body', $body);
    $config->save();

    parent::submitForm($form, $form_state);  
  }
}